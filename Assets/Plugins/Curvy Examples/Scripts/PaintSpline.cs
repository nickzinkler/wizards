// =====================================================================
// Copyright 2013-2018 ToolBuddy
// All rights reserved
// 
// http://www.toolbuddy.net
// =====================================================================

using UnityEngine;
using System.Collections;
using FluffyUnderware.Curvy;
using FluffyUnderware.DevTools.Extensions;
using FluffyUnderware.Curvy.Controllers;
using UnityEngine.UI;

/* 
 * In this example we let the user draw a spline on screen!
 * 
 */
namespace FluffyUnderware.Curvy.Examples
{
    public class PaintSpline : MonoBehaviour
    {
        [SerializeField] private Camera UICamera;
        [SerializeField] private RectTransform rectTransform;
        
        public float StepDistance = 30;
        public Text InfoText;

        CurvySpline mSpline;
        Vector2 mLastControlPointPos;
        bool mResetSpline = true;
        
        void Awake()
        {
            // for this example we assume the component is attached to a GameObject holding a spline
            mSpline = GetComponent<CurvySpline>();
        }

        void OnGUI()
        {
            // before using the spline, ensure it's initialized and the Controller is available
            if (mSpline == null || !mSpline.IsInitialized)
                return;

            var e = Event.current;
            switch (e.type)
            {
                case EventType.MouseDown:
                    addCP(e.mousePosition);
                    break;
                    
                case EventType.MouseDrag:
                    // Start a new line?
                    if (mResetSpline)
                    {
                        mSpline.Clear(); // delete all Control Points
                        addCP(e.mousePosition); // add the first Control Point
                        mLastControlPointPos = e.mousePosition; // Store current mouse position
                        mResetSpline = false;
                    }
                    else
                    {
                        // only create a new Control Point if the minimum distance is reached
                        float dist = (e.mousePosition - mLastControlPointPos).magnitude;
                        if (dist >= StepDistance)
                        {
                            mLastControlPointPos = e.mousePosition;
                            addCP(e.mousePosition);
                        }
                    }

                    break;
                case EventType.MouseUp:
                    mResetSpline = true;

                    break;
            }
        }

        // Add a Control Point and set it's position
        CurvySplineSegment addCP(Vector3 mousePos)
        {
            mSpline.Add();

            RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform, mousePos, UICamera,
                out var finalPos);
            //mousePos.y *= -1; // flip Y to get the correct world position
            //p.z += 100; //To move further than camera's plane. The value 100 comes from the Canvas' plane distance
            var cp = mSpline.InsertAfter(null, finalPos, false);

            // InfoText.text = "Control Points: " + mSpline.ControlPointCount.ToString(); // set info text

            return cp;
        }
    }
}
